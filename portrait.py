import tkinter as tk
from PIL import Image, ImageTk
import random

root = tk.Tk()

global cnv
cnv = tk.Canvas(root, bg ="lightgray", height = 500, width = 500)
#fonction visage alea avec random.randint 
def alea():
    global lunette,yeux,crane,cheveux,barbe,mustache,bouche,menton,sourcil,nez,oreilles,zoom
    cnv.delete("all")
    lunette = random.randint(160,194)
    yeux =  random.randint(345,366)
    crane = random.randint(376,384)
    cheveux = random.randint(10,67)
    barbe = random.randint(68,99)
    mustache = random.randint(125,159)
    bouche = random.randint(195,245)
    menton = random.randint(316,344)
    sourcil = random.randint(284,315)
    nez = random.randint(246,283)
    oreilles = random.randint(367,375)


    beard = tk.PhotoImage(file="221x347/beard/layer-0"+ str(barbe) + ".png")#68-124
    ears = tk.PhotoImage(file="221x347/ears/layer-"+ str(oreilles) + ".png")#367-375
    chin = tk.PhotoImage(file="221x347/chin/layer-"+ str(menton) + ".png")#316-344
    cranium = tk.PhotoImage(file="221x347/cranium/layer-"+ str(crane) + ".png")#376-384
    eyebrows = tk.PhotoImage(file="221x347/eyebrows/layer-"+ str(sourcil) + ".png")#284-315
    eyes = tk.PhotoImage(file="221x347/eyes/layer-"+ str(yeux) + ".png")#345-366
    glasses = tk.PhotoImage(file="221x347/glasses/layer-"+ str(lunette) + ".png")#160-194
    hair = tk.PhotoImage(file="221x347/hair/layer-0"+ str(cheveux) + ".png")#001-067
    moustache = tk.PhotoImage(file="221x347/moustache/layer-"+ str(mustache)+ ".png")#125-159
    mouth = tk.PhotoImage(file="221x347/mouth/layer-"+ str(bouche) + ".png")#195-245
    nose = tk.PhotoImage(file="221x347/nose/layer-"+ str(nez) + ".png")#246-283


    cnv.create_image(250, 240, image=beard)
    cnv.create_image(250, 250, image=ears)
    cnv.create_image(250, 250, image=chin)
    cnv.create_image(250, 260, image=cranium)
    cnv.create_image(250, 250, image=eyes)
    cnv.create_image(250, 250, image=eyebrows)
    cnv.create_image(250, 250, image=glasses)
    cnv.create_image(250, 260, image=hair)
    cnv.create_image(250, 250, image=mouth)
    cnv.create_image(250, 250, image=nose)
    cnv.create_image(250, 250, image=moustache)

    cnv.image1 = beard
    cnv.image2 = ears
    cnv.image3 = chin
    cnv.image4 = cranium
    cnv.image5 = eyes
    cnv.image6 = eyebrows
    cnv.image7 = glasses
    cnv.image8 = hair
    cnv.image9 = mouth
    cnv.image10 = nose
    cnv.image11 = moustache

    increment(zoom)

#fonction pour changer d'image en fesant +
def visage(val):
    global lunette,yeux,crane,cheveux,barbe,mustache,bouche,menton,sourcil,nez,oreilles,zoom
    if val == 'glasses':
        if lunette == 159:
            eyes = tk.PhotoImage(file="221x347/eyes/layer-"+ str(yeux) + ".png")#345-366
            cnv.create_image(250, 250, image=eyes)
            cnv.image7 = eyes
            cnv.image5 = eyes
            lunette = lunette +1
        elif lunette >= 194:
            lunette = 159
        elif lunette > 159 and lunette < 194:
            glasses = tk.PhotoImage(file="221x347/glasses/layer-"+ str(lunette) + ".png")#160-194
            cnv.create_image(250, 250, image=glasses)
            cnv.image7 = glasses
            lunette = lunette + 1
            increment(zoom)

    if val =='eyes':
        if yeux >= 366:
            yeux = 345
        eyes = tk.PhotoImage(file="221x347/eyes/layer-"+ str(yeux) + ".png")#345-366
        cnv.create_image(250, 250, image=eyes)
        cnv.image5 = eyes
        yeux = yeux + 1
        increment(zoom)

    if val =='eyebrows':
        if sourcil >= 315:
            sourcil = 284
        eyebrows = tk.PhotoImage(file="221x347/eyebrows/layer-"+ str(sourcil) + ".png")#284-315
        cnv.create_image(250, 250, image=eyebrows)
        cnv.image6 = eyebrows
        sourcil = sourcil + 1
        increment(zoom)

    if val =='beard':
        if barbe == 67 :
            chin = tk.PhotoImage(file="221x347/chin/layer-"+ str(menton) + ".png")#316-344
            cnv.create_image(250, 250, image=chin)
            cnv.image1 = chin
            barbe= barbe +1
        if barbe < 100 and barbe > 67:
            beard = tk.PhotoImage(file="221x347/beard/layer-0"+ str(barbe) + ".png")#68-124
            barbe = barbe + 1
            cnv.create_image(250, 240, image=beard)
            cnv.image1 = beard
            increment(zoom)
        if barbe > 99 and barbe < 124:
            beard = tk.PhotoImage(file="221x347/beard/layer-"+ str(barbe) + ".png")#68-124
            barbe = barbe + 1
            cnv.create_image(250, 240, image=beard)
            cnv.image1 = beard
            increment(zoom)
        if barbe >= 124:
            barbe = 67

    if val =='ears':
        if oreilles >= 375:
            oreilles = 368
        ears = tk.PhotoImage(file="221x347/ears/layer-"+ str(oreilles) + ".png")#367-375
        cnv.create_image(250, 250, image=ears)
        cnv.image2 = ears
        oreilles = oreilles + 1
        increment(zoom)

    if val =='chin':
        if menton >= 344:
            menton = 316
        chin = tk.PhotoImage(file="221x347/chin/layer-"+ str(menton) + ".png")#316-344
        cnv.create_image(250, 250, image=chin)
        cnv.image3 = chin
        menton = menton + 1
        increment(zoom)

    if val =='nose':
        if nez >= 283:
            nez = 246
        nose = tk.PhotoImage(file="221x347/nose/layer-"+ str(nez) + ".png")#246-283
        cnv.create_image(250, 250, image=nose)
        cnv.image10 = nose
        nez = nez + 1
        increment(zoom)

    if val =='mouth':
        if bouche >= 245:
            bouche = 195
        mouth = tk.PhotoImage(file="221x347/mouth/layer-"+ str(bouche) + ".png")#195-245
        cnv.create_image(250, 250, image=mouth)
        cnv.image9 = mouth
        bouche = bouche + 1
        increment(zoom)

    if val =='moustache':
        if mustache == 124:
            print(bouche)
            mouth = tk.PhotoImage(file="221x347/mouth/layer-"+ str(bouche) + ".png")#195-245
            cnv.create_image(250, 250, image=mouth)
            cnv.image11 = mouth
            mustache = mustache + 1
        if mustache >= 159:
            mustache = 124
        if mustache >124 and mustache<159:
            moustache = tk.PhotoImage(file="221x347/moustache/layer-"+ str(mustache)+ ".png")#125-159
            cnv.create_image(250, 250, image=moustache)
            cnv.image11 = moustache
            mustache = mustache + 1
            increment(zoom)

    if val =='hair':
        if cheveux == 0:
            cranium = tk.PhotoImage(file="221x347/cranium/layer-"+ str(crane) + ".png")#376-384
            cnv.create_image(250, 260, image=cranium)
            cnv.image8 = cranium
            cheveux = cheveux +1
        elif cheveux <= 9:
            hair = tk.PhotoImage(file="221x347/hair/layer-00"+ str(cheveux) + ".png")#001-067
            cnv.create_image(250, 260, image=hair)
            cnv.image8 = hair
            cheveux = cheveux + 1
            increment(zoom)
        elif cheveux >9 and cheveux < 67:               
            hair = tk.PhotoImage(file="221x347/hair/layer-0"+ str(cheveux) + ".png")#001-067
            cnv.create_image(250, 260, image=hair)
            cnv.image8 = hair
            cheveux = cheveux + 1
            increment(zoom)
        elif cheveux >= 67:
            cheveux = 0

    if val =='cranium':
        if crane >= 384:
            crane = 376
        cranium = tk.PhotoImage(file="221x347/cranium/layer-"+ str(crane) + ".png")#376-384
        cnv.create_image(250, 260, image=cranium)
        cnv.image4 = cranium
        crane = crane + 1
        increment(zoom)

#fonction décremente
def visageDecremente(val):
    global lunette,yeux,crane,cheveux,barbe,mustache,bouche,menton,sourcil,nez,oreilles,zoom
    if val == 'glasses':
        if lunette == 159:
            eyes = tk.PhotoImage(file="221x347/eyes/layer-"+ str(yeux) + ".png")#345-366
            cnv.create_image(250, 250, image=eyes)
            cnv.image7 = eyes
            cnv.image5 = eyes
            lunette = 194
        elif lunette >= 194:
            glasses = tk.PhotoImage(file="221x347/glasses/layer-"+ str(lunette) + ".png")#160-194
            cnv.create_image(250, 250, image=glasses)
            cnv.image7 = glasses
            lunette = lunette - 1
            increment(zoom)
        elif lunette > 159 and lunette < 194:
            glasses = tk.PhotoImage(file="221x347/glasses/layer-"+ str(lunette) + ".png")#160-194
            cnv.create_image(250, 250, image=glasses)
            cnv.image7 = glasses
            lunette = lunette - 1
            increment(zoom)

    if val =='eyes':
        if yeux <=345 :
            yeux = 366
        eyes = tk.PhotoImage(file="221x347/eyes/layer-"+ str(yeux) + ".png")#345-366
        cnv.create_image(250, 250, image=eyes)
        cnv.image5 = eyes
        yeux = yeux - 1
        increment(zoom)

    if val =='eyebrows':
        if sourcil <= 284 :
            sourcil = 315
        eyebrows = tk.PhotoImage(file="221x347/eyebrows/layer-"+ str(sourcil) + ".png")#284-315
        cnv.create_image(250, 250, image=eyebrows)
        cnv.image6 = eyebrows
        sourcil = sourcil - 1
        increment(zoom)
    

    if val =='beard':
        if barbe == 67 :
            chin = tk.PhotoImage(file="221x347/chin/layer-"+ str(menton) + ".png")#316-344
            cnv.create_image(250, 250, image=chin)
            cnv.image1 = chin
            barbe = 124
        elif barbe < 100 and barbe > 67:
            beard = tk.PhotoImage(file="221x347/beard/layer-0"+ str(barbe) + ".png")#68-124
            barbe = barbe - 1
            cnv.create_image(250, 240, image=beard)
            cnv.image1 = beard
            increment(zoom)
        elif barbe <= 124 and barbe >= 100:
            beard = tk.PhotoImage(file="221x347/beard/layer-"+ str(barbe) + ".png")#68-124
            barbe = barbe - 1
            cnv.create_image(250, 240, image=beard)
            cnv.image1 = beard
            increment(zoom)

    if val =='ears':
        if oreilles <= 368:
            oreilles = 375
        ears = tk.PhotoImage(file="221x347/ears/layer-"+ str(oreilles) + ".png")#367-375
        cnv.create_image(250, 250, image=ears)
        cnv.image2 = ears
        oreilles = oreilles - 1
        increment(zoom)

    if val =='chin':
        if menton <= 316:
            menton = 344
        chin = tk.PhotoImage(file="221x347/chin/layer-"+ str(menton) + ".png")#316-344
        cnv.create_image(250, 250, image=chin)
        cnv.image3 = chin
        menton = menton - 1
        increment(zoom)
    if val =='nose':
        if nez <= 246:
            nez = 283
        nose = tk.PhotoImage(file="221x347/nose/layer-"+ str(nez) + ".png")#246-283
        cnv.create_image(250, 250, image=nose)
        cnv.image10 = nose
        nez = nez - 1
        increment(zoom)

    if val =='mouth':
        if bouche <= 195:
            bouche = 245
        mouth = tk.PhotoImage(file="221x347/mouth/layer-"+ str(bouche) + ".png")#195-245
        cnv.create_image(250, 250, image=mouth)
        cnv.image9 = mouth
        bouche = bouche - 1
        increment(zoom)

    if val =='moustache':
        if mustache == 124:
            print(bouche)
            mouth = tk.PhotoImage(file="221x347/mouth/layer-"+ str(bouche) + ".png")#195-245
            cnv.create_image(250, 250, image=mouth)
            cnv.image11 = mouth
            mustache = 159
        elif mustache == 159:
            moustache = tk.PhotoImage(file="221x347/moustache/layer-"+ str(mustache)+ ".png")#125-159
            cnv.create_image(250, 250, image=moustache)
            cnv.image11 = moustache
            mustache = mustache - 1
            increment(zoom)
        elif mustache >124 and mustache<159:
            moustache = tk.PhotoImage(file="221x347/moustache/layer-"+ str(mustache)+ ".png")#125-159
            cnv.create_image(250, 250, image=moustache)
            cnv.image11 = moustache
            mustache = mustache - 1
            increment(zoom)

    if val =='hair':
        if cheveux == 0:
            cranium = tk.PhotoImage(file="221x347/cranium/layer-"+ str(crane) + ".png")#376-384
            cnv.create_image(250, 260, image=cranium)
            cnv.image8 = cranium
            cheveux = 67
        elif cheveux <= 9 and cheveux >0:
            hair = tk.PhotoImage(file="221x347/hair/layer-00"+ str(cheveux) + ".png")#001-067
            cnv.create_image(250, 260, image=hair)
            cnv.image8 = hair
            cheveux = cheveux - 1
            increment(zoom)
        elif cheveux >9 and cheveux <= 67:          
            hair = tk.PhotoImage(file="221x347/hair/layer-0"+ str(cheveux) + ".png")#001-067
            cnv.create_image(250, 260, image=hair)
            cnv.image8 = hair
            cheveux = cheveux - 1
            increment(zoom)
    
    if val =='cranium':
        if crane <= 376:
            crane = 384
        cranium = tk.PhotoImage(file="221x347/cranium/layer-"+ str(crane) + ".png")#376-384
        cnv.create_image(250, 260, image=cranium)
        cnv.image4 = cranium
        crane = crane - 1
        increment(zoom)

def increment(x):
    global lunette,yeux,crane,cheveux,barbe,mustache,bouche,menton,sourcil,nez,oreiles,zoom
    zoom = x
    cnv.delete("all")

    beard = tk.PhotoImage(file="221x347/beard/layer-0"+ str(barbe) + ".png")#68-124
    ears = tk.PhotoImage(file="221x347/ears/layer-"+ str(oreilles) + ".png")#367-375
    chin = tk.PhotoImage(file="221x347/chin/layer-"+ str(menton) + ".png")#316-344
    cranium = tk.PhotoImage(file="221x347/cranium/layer-"+ str(crane) + ".png")#376-384
    eyebrows = tk.PhotoImage(file="221x347/eyebrows/layer-"+ str(sourcil) + ".png")#284-315
    eyes = tk.PhotoImage(file="221x347/eyes/layer-"+ str(yeux) + ".png")#345-366
    glasses = tk.PhotoImage(file="221x347/glasses/layer-"+ str(lunette) + ".png")#160-194
    hair = tk.PhotoImage(file="221x347/hair/layer-0"+ str(cheveux) + ".png")#001-067
    moustache = tk.PhotoImage(file="221x347/moustache/layer-"+ str(mustache)+ ".png")#125-159
    mouth = tk.PhotoImage(file="221x347/mouth/layer-"+ str(bouche) + ".png")#195-245
    nose = tk.PhotoImage(file="221x347/nose/layer-"+ str(nez) + ".png")#246-283

    if x == 2:
        cnv.configure(width=1000, height=1000)
    else:
        cnv.configure(width=500, height=500)

    cranium = cranium.zoom(x=x, y=x)
    chin = chin.zoom(x=x, y=x)
    eyebrows = eyebrows.zoom(x=x, y=x)
    eyes = eyes.zoom(x=x, y=x)
    ears = ears.zoom(x=x, y=x)
    beard = beard.zoom(x=x, y=x)
    glasses = glasses.zoom(x=x, y=x)
    hair = hair.zoom(x=x, y=x)
    glasses = glasses.zoom(x=x, y=x)
    moustache = moustache.zoom(x=x, y=x)
    mouth = mouth.zoom(x=x, y=x)
    nose = nose.zoom(x=x, y=x)

    cnv.create_image((250*x), (x*240), image=beard)
    cnv.create_image((250*x), (250*x), image=ears)
    cnv.create_image((250*x), (250*x), image=chin)
    cnv.create_image((250*x), (x*260), image=cranium)
    cnv.create_image((250*x), (250*x), image=eyes)
    cnv.create_image((250*x), (250*x), image=eyebrows)
    cnv.create_image((250*x), (250*x), image=glasses)
    cnv.create_image((250*x), (x*260), image=hair)
    cnv.create_image((250*x), (250*x), image=mouth)
    cnv.create_image((250*x), (250*x), image=nose)
    cnv.create_image((250*x), (250*x), image=moustache)

    cnv.image1 = beard
    cnv.image2 = ears
    cnv.image3 = chin
    cnv.image4 = cranium
    cnv.image5 = eyes
    cnv.image6 = eyebrows
    cnv.image7 = glasses
    cnv.image8 = hair
    cnv.image9 = mouth
    cnv.image10 = nose
    cnv.image11 = moustache




btn_increment = tk.Button(root, width=3, height=1, text="zoom-x2",command = lambda : increment(2))




btn_increment.place(x=0, y=350)
 
#variable pour incrementé
global lunette,yeux,crane,cheveux,barbe,mustache,bouche,menton,sourcil,nez,oreilles,zoom

lunette = 159
yeux = 345
crane = 376
cheveux = 0
mustache = 124
nez = 246
menton = 316
oreilles = 367
barbe = 68
sourcil = 284
bouche = 195
zoom = 1

#bouton alea
btn = tk.Button(root, width=14, height=1, text="aléa",command = lambda : alea())

#bouton incrément
btn_lunette = tk.Button(root, width=8, height=1, text="glasses",command = lambda : visage('glasses'))
btn_eyes = tk.Button(root, width=8, height=1, text="eyes",command = lambda : visage('eyes'))
btn_cranium = tk.Button(root, width=8, height=1, text="cranium",command = lambda : visage('cranium'))
btn_nose = tk.Button(root, width=8, height=1, text="nose",command = lambda : visage('nose'))
btn_moustache = tk.Button(root, width=8, height=1, text="moustache",command = lambda : visage('moustache'))
btn_beard = tk.Button(root, width=8, height=1, text="beard",command = lambda : visage('beard'))
btn_eyebrows = tk.Button(root, width=8, height=1, text="eyebrows",command = lambda : visage('eyebrows'))
btn_ears = tk.Button(root, width=8, height=1, text="ears",command = lambda : visage('ears'))
btn_chin = tk.Button(root, width=8, height=1, text="chin",command = lambda : visage('chin'))
btn_mouth = tk.Button(root, width=8, height=1, text="mouth",command = lambda : visage('mouth'))
btn_hair = tk.Button(root, width=8, height=1, text="hair",command = lambda : visage('hair'))

#bouton décremente
décremente_btn_lunette = tk.Button(root, width=3, height=1, text="◀️",command = lambda : visageDecremente('glasses'))
décremente_btn_eyes = tk.Button(root, width=3, height=1, text="◀️",command = lambda : visageDecremente('eyes'))
décremente_btn_cranium = tk.Button(root, width=3, height=1, text="◀️",command = lambda : visageDecremente('cranium'))
décremente_btn_nose = tk.Button(root, width=3, height=1, text="◀️",command = lambda : visageDecremente('nose'))
décremente_btn_moustache = tk.Button(root, width=3, height=1, text="◀️",command = lambda : visageDecremente('moustache'))
décremente_btn_beard = tk.Button(root, width=3, height=1, text="◀️",command = lambda : visageDecremente('beard'))
décremente_btn_eyebrows = tk.Button(root, width=3, height=1, text="◀️",command = lambda : visageDecremente('eyebrows'))
décremente_btn_ears = tk.Button(root, width=3, height=1, text="◀️",command = lambda : visageDecremente('ears'))
décremente_btn_chin = tk.Button(root, width=3, height=1, text="◀️",command = lambda : visageDecremente('chin'))
décremente_btn_mouth = tk.Button(root, width=3, height=1, text="◀️",command = lambda : visageDecremente('mouth'))
décremente_btn_hair = tk.Button(root, width=3, height=1, text="◀️",command = lambda : visageDecremente('hair'))

#placement btn
btn.place(x = 0 ,y= 50)
btn_eyes.place(x=0,y=100)
btn_lunette.place(x=0,y = 75)
btn_beard.place(x=0,y=125)
btn_chin.place(x=0,y=150)
btn_cranium.place(x=0,y=175)
btn_ears.place(x=0,y=200)
btn_eyebrows.place(x=0,y=225)
btn_hair.place(x=0,y=250)
btn_moustache.place(x=0,y=275)
btn_mouth.place(x=0,y=300)
btn_nose.place(x=0,y=325)

#placement btn back
décremente_btn_eyes.place(x=90,y=100)
décremente_btn_lunette.place(x=90,y = 75)
décremente_btn_beard.place(x=90,y=125)
décremente_btn_chin.place(x=90,y=150)
décremente_btn_cranium.place(x=90,y=175)
décremente_btn_ears.place(x=90,y=200)
décremente_btn_eyebrows.place(x=90,y=225)
décremente_btn_hair.place(x=90,y=250)
décremente_btn_moustache.place(x=90,y=275)
décremente_btn_mouth.place(x=90,y=300)
décremente_btn_nose.place(x=90,y=325)


btn_increment = tk.Button(root, width=8, height=1, text="zoom",command = lambda : increment(2))
btn_increment.place(x=0, y=350)
btn_dezoom = tk.Button(root, width=8, height=1, text="dézoom",command = lambda : increment(1))
btn_dezoom.place(x=90, y=350)
cnv.pack()
root.mainloop()